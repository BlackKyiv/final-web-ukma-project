import './App.css';
import MainPage from './components/MainPage';
import Login from './components/Login';
import {
  BrowserRouter,
  Route,
  Routes,
} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { UserState } from './contexts/Context';
import NavHeader from './components/NavHeader';
import Register from './components/Register';
import Contacts from './components/Contacts';

function App() {
  return (
    <div className="back-ground">
      <BrowserRouter>
        <NavHeader />
        <div className="main-content">
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/contacts" element={<Contacts />} />
          </Routes>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
