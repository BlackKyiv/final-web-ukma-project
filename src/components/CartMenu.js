import { DropdownButton, Button, NavDropdown } from 'react-bootstrap';
import { UserState } from '../contexts/Context';

const CartMenu = ({}) => {
  const { cart, cartControll } = UserState();
  return (
    <NavDropdown title={<CartIcon />} menuVariant="dark">
      {cart.map((i) => {
        return (
          <NavDropdown.Item key={'drop' + i.id}>
            {<CartItem key={'cart' + i.id} item={i} />}
          </NavDropdown.Item>
        );
      })}

      <NavDropdown.Divider />
      <NavDropdown.Item onClick={() => cartControll({ type: 'CLEAR' })}>
        Clear Cart
      </NavDropdown.Item>
    </NavDropdown>
  );
};

const CartItem = ({ item }) => {
  const { cartControll } = UserState();
  return (
    <div className="item-card-item">
      {item.name}
      <Button
        variant="danger"
        size="sm"
        onClick={() => cartControll({ type: 'REMOVE', payload: item.id })}
      >
        X
      </Button>
    </div>
  );
};

const CartIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="30"
      height="30"
      fill="currentColor"
      className="bi bi-cart"
      viewBox="0 0 16 16"
    >
      <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
    </svg>
  );
};

const CartRemoveIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="20"
      fill="currentColor"
      class="bi bi-cart-x-fill"
      viewBox="0 0 16 16"
    >
      <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zM7.354 5.646 8.5 6.793l1.146-1.147a.5.5 0 0 1 .708.708L9.207 7.5l1.147 1.146a.5.5 0 0 1-.708.708L8.5 8.207 7.354 9.354a.5.5 0 1 1-.708-.708L7.793 7.5 6.646 6.354a.5.5 0 1 1 .708-.708z" />
    </svg>
  );
};

export default CartMenu;
