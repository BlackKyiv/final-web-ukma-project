import { InputGroup, Button, Form, Spinner, Accordion } from 'react-bootstrap';
import { UserState } from '../contexts/Context';
import axios from 'axios';
import React, { useState } from 'react';

const Comments = ({ id }) => {
  const { user } = UserState();
  const [comments, setComments] = React.useState(null);
  const getComments = () => handleGettingComments(setComments, id);

  React.useEffect(() => getComments(), []);

  const addComment = () => {
    return (
      <div className="add-comment-box">
        <Form onSubmit={(e) => handleAddingComment(e, user, id, getComments)}>
          <InputGroup>
            <Form.Control
              type="text"
              name="text"
              placeholder="Add your comment"
            />
            <Button variant="success" type="submit">
              Post
            </Button>
          </InputGroup>
        </Form>
      </div>
    );
  };

  return (
    <div>
      <hr />
      <Accordion>
        <Accordion.Item eventKey="0">
          <Accordion.Header>Show Comments</Accordion.Header>
          <Accordion.Body>
            {user.loggedIn && addComment()}
            {comments ? (
              <DisplayComments comments={comments} />
            ) : (
              <div className="spinner-main">
                <Spinner animation="border" role="status" className="spinner" />
              </div>
            )}
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    </div>
  );
};

const DisplayComments = ({ comments }) => {
  var i = 0;
  return (
    <div>
      {comments
        ?.map((c) => {
          const k = `${i++}comments`;
          return <Comment key={k} comment={c} />;
        })
        .reverse()}
    </div>
  );
};

const Comment = ({ comment }) => {
  return (
    <div className="comment">
      <p>
        <b>{comment.name}</b>
      </p>
      <hr />
      <p>{comment.text}</p>
    </div>
  );
};

function handleGettingComments(setComments, id) {
  const base_url = 'http://localhost:3000/comments';
  axios
    .get(base_url, {
      params: {
        item_id: id,
      },
    })
    .then((resp) => {
      setComments(resp.data);
    });
}

function handleAddingComment(e, user, id, getComments) {
  e.preventDefault();
  console.log({
    item_id: id,
    user_id: user.user.id,
    name: user.user.login,
    text: e.target.text.value,
  });
  console.log(e);
  const base_url = 'http://localhost:3000/comments';
  axios
    .post(base_url, {
      item_id: id,
      user_id: user.user.id,
      name: user.user.login,
      text: e.target.text.value,
    })
    .then((resp) => getComments());
}

export default Comments;
