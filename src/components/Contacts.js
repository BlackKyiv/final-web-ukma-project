import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import { useNavigate } from 'react-router-dom';
import React, { useState } from 'react';
import { Container, Row, Col, Spinner } from 'react-bootstrap';
import axios from 'axios';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import L from 'leaflet';
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';

const DefaultIcon = L.icon({
  iconUrl: icon,
  shadowUrl: iconShadow,
});

L.Marker.prototype.options.icon = DefaultIcon;

const Contacts = () => {
  const [isSending, setSending] = useState(false);

  return (
    <Container className="contacts-container">
      <Row>
        <div className="header-contacts">
          <br />
          <h2>Contact Us</h2>
          <Map />
        </div>
      </Row>
      <Row>
        <Col>
          <Form onSubmit={(e) => handleSendingMessage(e, setSending)}>
            <Form.Group className="App-Login-Group">
              <Form.Label>Your name</Form.Label>
              <Form.Control type="text" name="name" required />
            </Form.Group>

            <Form.Group className="App-Login-Group">
              <Form.Label>Your email</Form.Label>
              <Form.Control type="text" name="email" required />
            </Form.Group>

            <Form.Group className="App-Login-Group">
              <Form.Label>Your message</Form.Label>
              <Form.Control as="textarea" name="message" rows={10} required />
            </Form.Group>
            <br />
            {isSending ? (
              <Spinner animation="border" role="status" className="spinner" />
            ) : (
              <Button variant="success" type="submit">
                Send
              </Button>
            )}
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

function Map() {
  const location = [51.509865, -0.118092];
  return (
    <div className="leaflet-container">
      <MapContainer center={location} zoom={12} scrollWheelZoom={false}>
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />
        <Marker position={location}>
          <Popup>We are here!</Popup>
        </Marker>
      </MapContainer>
    </div>
  );
}

function handleSendingMessage(e, setSaving) {
  e.preventDefault();
  const base_url = 'http://localhost:3000/messages';
  const message = {
    name: e.target.name.value,
    email: e.target.email.value,
    message: e.target.message.value,
  };
  setSaving(true);
  axios.post(base_url, message).then((resp) => {
    setSaving(false);
  });
}

export default Contacts;
