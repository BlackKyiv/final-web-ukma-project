import {
  InputGroup,
  FormControl,
  Button,
  DropdownButton,
  Dropdown,
  Form,
} from 'react-bootstrap';
import { UserState } from '../contexts/Context';

const Filters = () => {
  const { filters, setCurrentPage, filterControll } = UserState();
  return (
    <div className="filters-container">
      <Form>
        <InputGroup className="mb-3">
          <InputGroup.Text>Only in stock:</InputGroup.Text>
          <InputGroup.Checkbox
            checked={filters.filterByInStock}
            onChange={(e) => {
              setCurrentPage(1);
              filterControll({
                type: 'SET_STOCK_FILTER',
                payload: e.target.checked,
              });
            }}
          />
        </InputGroup>

        <InputGroup className="mb-3 search-input">
          <InputGroup.Text id="inputGroup-sizing-default">
            Search:
          </InputGroup.Text>
          <FormControl
            placeholder="Search..."
            value={filters.search ? filters.search : ''}
            onChange={(e) => {
              setCurrentPage(1);
              filterControll({ type: 'SET_SEARCH', payload: e.target.value });
            }}
          />
        </InputGroup>

        <InputGroup className="mb-3">
          <InputGroup.Text>Sort:</InputGroup.Text>
          <DropdownButton
            variant="outline-secondary"
            title={filters.sort ? filters.sort : '--'}
          >
            <Dropdown.Item
              value="none"
              onClick={(e) =>
                filterControll({ type: 'SET_SORT', payload: 'None' })
              }
            >
              None
            </Dropdown.Item>
            <Dropdown.Item
              onClick={(e) =>
                filterControll({ type: 'SET_SORT', payload: 'From high price' })
              }
            >
              From highest price
            </Dropdown.Item>
            <Dropdown.Item
              value="fromLow"
              onClick={(e) =>
                filterControll({ type: 'SET_SORT', payload: 'From low price' })
              }
            >
              From lowest price
            </Dropdown.Item>
          </DropdownButton>
        </InputGroup>

        <InputGroup className="mb-3">
          <Button
            variant="outline-secondary"
            onClick={() => filterControll({ type: 'CLEAR' })}
          >
            Clear Filters
          </Button>
        </InputGroup>
      </Form>
      <hr />
    </div>
  );
};

export default Filters;
