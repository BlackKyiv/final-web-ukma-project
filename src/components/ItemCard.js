import { Button, Card } from 'react-bootstrap';
import React from 'react';
import ItemModal from './ItemModal';
import { UserState } from '../contexts/Context';

const ItemCard = ({ item }) => {
  const [modalIsOpen, setIsOpen] = React.useState(false);
  const { cartControll } = UserState();
  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  const addToCart = () => {
    cartControll({
      type: 'ADD',
      payload: item,
    });
  };

  return (
    <>
      <ItemModal
        item={item}
        isOpen={modalIsOpen}
        closeModal={closeModal}
        AddToCartButton={AddToCartButton}
        addToCart={addToCart}
      />
      <Card style={{ width: '18rem', margin: '1em' }}>
        <Card.Img variant="top" src={item.image} />
        <Card.Body>
          <Card.Title>{item.name}</Card.Title>
          <Card.Text>{item.price}$</Card.Text>
          <div className="card-buttons">
            {item.inStock && <AddToCartButton item={item} action={addToCart} />}
            <SeeButton openModal={openModal} />
          </div>
        </Card.Body>
      </Card>
    </>
  );
};

const AddToCartButton = ({ size = 'sm', item, action }) => {
  if (!item.inStock) {
    return (
      <Button variant="outline-secondary" size={size} disabled>
        Out of stock
      </Button>
    );
  }
  const { cart } = UserState();
  if (cart.includes(item)) {
    return (
      <Button variant="outline-secondary" size={size} disabled>
        In Cart
      </Button>
    );
  }
  return (
    <Button variant="primary" size={size} onClick={action}>
      Add to Cart
    </Button>
  );
};

const SeeButton = ({ openModal }) => {
  return (
    <Button variant="outline-primary" size="sm" onClick={openModal}>
      Details
    </Button>
  );
};

export default ItemCard;
