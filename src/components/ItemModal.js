import React from 'react';
import { Modal, Row, Col, Container } from 'react-bootstrap';
import Comments from './Comments';

const ItemModal = ({ item, isOpen, closeModal, AddToCartButton, addToCart }) => {
  return (
    <Modal show={isOpen} onHide={closeModal} size="lg" centered>
      <Modal.Header closeButton>
        <Modal.Title>{item.name}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="modal-box">
          <Container>
            <Row>
              <Col>
                <img src={item.image} className="img-m" />
              </Col>
              <Col>
                <div className="modal-data">
                  <p>
                    <b>Name: </b>
                    {item.name}
                  </p>
                  <p>
                    <b>Price: </b>
                    {item.price}$
                  </p>
                  {item.inStock && (
                    <p>
                      <b>In stock </b>
                    </p>
                  )}
                  <AddToCartButton action={addToCart} item={item}  size="lg" />
                </div>
              </Col>
            </Row>
          </Container>
        </div>
        <p className="description">
          <b>Description:</b>
        </p>
        <p className="descrip">{item.description}</p>
        <div>
          <Comments id={item.id} />
        </div>
      </Modal.Body>
    </Modal>
  );
};

export default ItemModal;
