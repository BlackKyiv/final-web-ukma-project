import React from 'react';
import Filters from './Filters.js';
import { Spinner, Row, Col, Container, Carousel } from 'react-bootstrap';
import ItemCard from './ItemCard.js';
import { UserState } from '../contexts/Context';
import Paging from './Paging.js';
import axios from 'axios';

const MainPage = () => {
  const { filters, currentPage, setAllPages, cart } = UserState();
  const [items, setItems] = React.useState(null);

  const generateUrl = () => {
    const baseUrl = 'http://localhost:3000/items?';
    const pageLimitItems = 6;
    let url = baseUrl;
    if (filters.sort === 'From high price') {
      url += '_sort=price&_order=desc&';
    } else if (filters.sort === 'From low price') {
      url += '_sort=price&_order=asc&';
    }
    if (filters.search && filters.search !== '') {
      url += `q=${filters.search}&`;
    }
    if (filters.filterByInStock) {
      url += `inStock=true&`;
    }
    url += `_page=${currentPage}&_limit=${pageLimitItems}`;

    return url;
  };

  React.useEffect(() => {
    axios.get(generateUrl()).then((resp) => {
      setAllPages(Math.ceil(resp.headers['x-total-count'] / 6));
      setItems(resp.data);
    });
  }, [filters, currentPage]);

  return (
    <div>
      <CarouselDisplay />
      <br />
      <Filters />
      {!items ? (
        <div className="spinner-main">
          <Spinner animation="border" role="status" className="spinner" />
        </div>
      ) : (
        <Items items={items} />
      )}
      <Paging />
    </div>
  );
};

function CarouselDisplay() {
  return (
    <div className="overflow-hidden">
      <Carousel className="carousel">
        <Carousel.Item className="item">
          <img
            className="d-block w-100 justify-content-center"
            src="https://bit.ly/31MJUmP"
            alt="First slide"
          />
        </Carousel.Item>
        <Carousel.Item className="item">
          <img
            className="d-block w-100"
            src="https://bit.ly/3rNccZv"
            alt="First slide"
          />
        </Carousel.Item>
        <Carousel.Item className="item">
          <img
            className="d-block w-100"
            src="https://bit.ly/3DCa8Wy"
            alt="First slide"
          />
        </Carousel.Item>
      </Carousel>
    </div>
  );
}

function Items({ items }) {
  return (
    <Container className="item-container">
      <Row className="justify-content-md-center">
        {items.map((item) => (
          <Col className="paging" key={'col' + item.id}>
            <ItemCard item={item} key={'item' + item.id} />
          </Col>
        ))}
      </Row>
    </Container>
  );
}

export default MainPage;
