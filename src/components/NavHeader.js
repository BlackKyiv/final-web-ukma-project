import { Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import { UserState } from '../contexts/Context';
import CartMenu from './CartMenu.js';


const NavHeader = () => {
  const { user } = UserState();
  return (
    <Navbar bg="dark" variant="dark" fixed="top">
      <div className="nav-container">
        <div className="nav-left">
          <Navbar.Text>
            <CartMenu />
          </Navbar.Text>
          <Navbar.Text>
            <Link to="/" className="nav-link-text">
              Store
            </Link>
          </Navbar.Text>
          <Navbar.Text>
            <Link to="/contacts" className="nav-link-text">
              Contacts
            </Link>
          </Navbar.Text>
        </div>
        <div className="nav-left">
          <AuthButtons />
        </div>
      </div>
    </Navbar>
  );
};

const AuthButtons = () => {
  const { user, userControll } = UserState();
  const logout = () => {
    userControll({ type: 'LOGOUT' });
  };

  console.log(user);
  if (user.loggedIn) {
    return (
      <span>
        <div className="nav-right">
          <Navbar.Text> You're logged in as {user.user.login} &nbsp;</Navbar.Text>
          <Button variant="outline-danger" onClick={logout}>
            Logout
          </Button>
        </div>
      </span>
    );
  } else {
    return (
      <span>
        <div className="nav-right">
          <Link to="/login" className="nav-link-text">
            <Button variant="outline-success">Login</Button>
          </Link>
          <Link to="/register" className="nav-link-text">
            <Button variant="outline-primary">Register</Button>
          </Link>
        </div>
      </span>
    );
  }
};

export default NavHeader;
