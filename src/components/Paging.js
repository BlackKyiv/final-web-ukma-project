import { InputGroup, Button } from 'react-bootstrap';
import { UserState } from '../contexts/Context';

const Paging = () => {
  const { currentPage, setCurrentPage, allPages } = UserState();

  const nums = [];
  for (let i = 1; i <= allPages; i++) {
    nums.push(i);
  }

  return (
    <>
      <br />
      <InputGroup className="paging">
        {nums.map((i) => (
          <Button
            variant={i === currentPage ? 'primary' : 'outline-primary'}
            onClick={() => setCurrentPage(i)}
            key={i + 'page'}
          >
            {i}
          </Button>
        ))}
      </InputGroup>
      <br />
    </>
  );
};

export default Paging;
