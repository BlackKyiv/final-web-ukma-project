import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import { useNavigate } from 'react-router-dom';
import React from 'react';
import { UserState } from '../contexts/Context';

const Register = () => {
  const { userControll } = UserState();
  const navigate = useNavigate();
  const [errorMessage, setErrorMessage] = React.useState(false);
  const showErrorMessage = () => {
    setErrorMessage(true);
  };
  const login = (user) => {
    userControll({
      type: 'LOGIN',
      payload: user,
    });
  };

  return (
    <div>
      <ErrorMessage show={errorMessage} />
      <Form
        className="App-Login"
        onSubmit={(e) =>
          handleRegisterAttempt(e, navigate, login, showErrorMessage)
        }
      >
        <Form.Group className="App-Login-Group" controlId="loginInput">
          <Form.Label>New Login</Form.Label>
          <Form.Control
            type="text"
            name="login"
            placeholder="Enter login"
            required
          />
        </Form.Group>

        <Form.Group className="App-Login-Group" controlId="passInput">
          <Form.Label>New Password</Form.Label>
          <Form.Control
            type="password"
            name="password"
            placeholder="Password"
            required
          />
        </Form.Group>

        <Form.Group className="App-Login-Group" controlId="passInput">
          <Form.Label>Repeat Password</Form.Label>
          <Form.Control
            type="password"
            name="passwordRepeat"
            placeholder="Password"
            required
          />
        </Form.Group>
        <Button variant="warning" type="submit">
          Register
        </Button>
      </Form>
    </div>
  );
};

function ErrorMessage({ show }) {
  return (
    <Alert className="delay-alert" hidden={!show} variant="danger">
      <h1>You've entered wrong login or password</h1>
    </Alert>
  );
}

function handleRegisterAttempt(e, navigate, login, showErrorMessage) {
  e.preventDefault();
  const base_url = 'http://localhost:3000/users';
  const user = {
    login: e.target.login.value,
    password: e.target.password.value,
  };

  fetch(base_url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(user),
  })
    .then((resp) => {
      return resp.json();
    })
    .then((data) => {
      if (data) {
        login(data);
        navigate('/profile');
      } else showErrorMessage();
    })
    .catch(() => {
      showErrorMessage();
    });
}

export default Register;
