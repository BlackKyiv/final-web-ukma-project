import { createContext, useContext, useReducer, useState } from 'react';

import { userController, filterController, cartController } from './Reducers';

const UserContext = createContext();

const Context = ({ children }) => {
  const [user, userControll] = useReducer(userController, {
    loggedIn: false,
    user: null,
  });

  const [filters, filterControll] = useReducer(filterController, {
    sort: 'None',
    filterByInStock: false,
    search: null,
  });

  const [currentPage, setCurrentPage] = useState(1);

  const [allPages, setAllPages] = useState(1);

  const [cart, cartControll] = useReducer(cartController, []);

  return (
    <UserContext.Provider
      value={{
        user,
        userControll,
        filters,
        filterControll,
        currentPage,
        setCurrentPage,
        allPages,
        setAllPages,
        cart,
        cartControll
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export const UserState = () => {
  return useContext(UserContext);
};

export default Context;
