export const userController = (state, action) => {
  switch (action.type) {
    case 'LOGOUT':
      return { loggedIn: false, user: null };
    case 'LOGIN':
      if (!action.payload) return { loggedIn: false, user: null };
      return {
        loggedIn: true,
        user: action.payload,
      };
  }
};

export const filterController = (state, action) => {
  switch (action.type) {
    case 'SET_STOCK_FILTER':
      return { ...state, filterByInStock: action.payload };
    case 'SET_SORT':
      return { ...state, sort: action.payload };
    case 'SET_SEARCH':
      return { ...state, search: action.payload };
    case 'CLEAR':
      return { sort: 'None', filterByInStock: false, search: null };
    default:
      return state;
  }
};

export const cartController = (state, action) => {
  switch (action.type) {
    case 'CLEAR':
      return [];
    case 'REMOVE':
      if (state.length === 1 && state[0].id === action.payload) return [];
      return state.filter((i) => i.id === action.payload);
    case 'ADD':
      if (action.payload?.inStock && !state.includes(action.payload)) {
        return state.concat([action.payload]);
      }
    default:
      return state;
  }
};
